(function ($) {
	$(document).ready(function () {
		$('#fullpage').fullpage({
			sectionsColor: ['#0B1B34', '#ffffff', '#ffffff', '#4fd899', '#ffffff', '#ffffff', '#ffffff', '#4fd899', '#ffffff', '#0B1B34'],
			anchors: ['home', 'introduction', 'problem', 'answer', 'relief', 'story','nowadays', 'unravel', 'lastwords',  'contact'],
			menu: '.menu-bar',
			navigation: true,
			navigationPosition: 'right',
			touchSensitivity: 8,
			responsiveWidth: 1023,
			navigationTooltips: ['home', 'introduction', 'problem', 'answer', 'relief', 'story','nowadays', 'unravel', 'lastwords',   'contact'],
			onLeave: function(index, nextIndex, direction){
				console.log(index);
			     switch(nextIndex) {
			         case 1:
			             document.title = "Home";
			             break;
			        case 2:
			             document.title = "Introduction";
			             break;
			        case 3:
			             document.title = "Problem";
			             break;
			        case 4:
			             document.title = "Answer";
			             break;
			        case 5:
			             document.title = "Relief";
			             break;
			        case 6:
			             document.title = "Story";
			             break;
			        case 7:
			             document.title = "Nowadays";
			             break;
			        case 8:
			             document.title = "Unravel";
			             break;
			        case 9:
			             document.title = "Lastwords";
			             break;
			        case 10:
			             document.title = "Contact";
			             break;
			        
			     }
	            var leavingSection = $(this);
	            // console.log(nextIndex);
	            //after leaving section 2
	            if(index >= 1 && direction =='down'){
	                $('.menu-btn img').attr('src','img/humburger_icon_blue.svg');
	            }
	            else if(index <= 2 && direction == 'up'){
	                $('.menu-btn img').attr('src','img/humburger_icon.svg');
	            }
	            else if(index >= 5 && direction == 'down'){
	                $('.menu-btn img').attr('src','img/humburger_icon.svg');
	            }

	            if(index != 7 ){
	            	$('#path-99, #path-104, #path-102, #path-105, #path-101').removeClass('fast');
	            }
	            if(index != 5 ){
	            	$('#clock').removeClass('fast');
	            }
	            if(index != 9 ){
	            	$('.last-word-table').removeClass('anim-2');
	            }
	            if(index <=5  && direction == 'up'){
	            	Tawk_API.maximize();
	            }
	            if(index >5  && direction == 'down'){
	            	$('body').removeClass('popup-in');
	            }
	            // if(nextIndex === 3 && direction == 'down'){
	            // 	setTimeout(function () {
	            // 		$('#section3 h3').addClass('delay-6s');
	            // 		$('.left-img, .right-img').addClass('op0');
	            // 	}, 5000);
	            // 	setTimeout(function () {
	            // 		$('.left-img-2, .right-img-2').addClass('op1');
	            // 	}, 7000);
	            // 	setTimeout(function () {
	            // 		$('#section3 h3').removeClass('delay-6s');
	            // 	}, 10000);
	            // }else{
	            // 		$('#section3 h3').removeClass('delay-6s');
	            // 		$('.left-img, .right-img').removeClass('op0');
	            // 		$('.left-img-2, .right-img-2').removeClass('op1');
	            // }
	            // if(nextIndex === 3 && direction == 'up'){
	            // 	setTimeout(function () {
	            // 		$('#section3 h3').addClass('delay-6s');
	            // 		$('.left-img, .right-img').addClass('op0');
	            // 	}, 5000);
	            // 	setTimeout(function () {
	            // 		$('.left-img-2, .right-img-2').addClass('op1');
	            // 	}, 8000);
	            // 	setTimeout(function () {
	            // 		$('#section3 h3').removeClass('delay-6s');
	            // 	}, 10000);
	            // }
	            // if(nextIndex == 2 && direction == 'down'){
	            // 	setTimeout(function () {
	            // 		$('#section1').addClass('animateIt');
	            // 	}, 3000);
	            // 	setTimeout(function () {
	            // 		$('.section1-1').fadeOut();
	            // 	}, 6000);
	            // 	setTimeout(function () {
	            // 		$('.section1-2').fadeIn(300, function () {
	            // 			$('.section1-2').addClass('animate');
	            // 		});
	            // 	}, 6500);

	            // }else if(index == 3 && direction == 'up'){
	            // 	setTimeout(function () {
	            // 		$('#section1').addClass('animateIt');
	            // 	}, 3000);
	            // 	setTimeout(function () {
	            // 		$('.section1-1').fadeOut();
	            // 	}, 6000);
	            // 	setTimeout(function () {
	            // 		$('.section1-2').fadeIn(300, function () {
	            // 			$('.section1-2').addClass('animate');
	            // 		});
	            // 	}, 6500);
	            // }
	            // else{
	            // 	$('#section1').removeClass('animateIt');
            	// 	$('.section1-1').show();
            	// 	$('.section1-2').hide();
            	// 	$('.section1-2').removeClass('animate');
	            // }


	        }
		});

		var Year = new Date().getFullYear();
		var developed_since = Year - 1993;
		$(".year-2").text(developed_since);

		$('.get-start-btn-2, .get-start-btn, .get-start-btn-3').on('click', function (e) {
			e.preventDefault();
			if(!$('body').hasClass('slideIn')){
				$.fn.fullpage.moveSectionDown();
			}
		});
		$('.menu-btn').on('click', function (e) {
			e.preventDefault();
			if($(this).hasClass("nav_close")){
				$(this).addClass('nav_open').removeClass('nav_close');
				$.fn.fullpage.setMouseWheelScrolling(false);
				$.fn.fullpage.setAllowScrolling(false);
				$('body').addClass('slideIn');
			}
			else if($(this).hasClass("nav_open")){
				$(this).addClass('nav_close').removeClass('nav_open');
				$.fn.fullpage.setMouseWheelScrolling(true);
				$.fn.fullpage.setAllowScrolling(true);
				$('body').removeClass('slideIn');
			}
		});
		$('.menu-bar a').on('click', function () {
			console.log($('.menu-btn').children().attr('src'));
			$(this).parent('li').addClass('active').siblings().removeClass('active');
			
			if($('.menu-btn').hasClass("nav_close")){
				$('.menu-btn').addClass('nav_open').removeClass('nav_close');
				$.fn.fullpage.setMouseWheelScrolling(false);
				$.fn.fullpage.setAllowScrolling(false);
				$('body').addClass('slideIn');
			}
			else if($('.menu-btn').hasClass("nav_open")){
				$('.menu-btn').addClass('nav_close').removeClass('nav_open');
				$.fn.fullpage.setMouseWheelScrolling(true);
				$.fn.fullpage.setAllowScrolling(true);
				$('body').removeClass('slideIn');
			}
		});
		
		$('.blub-img').on('click', function (e) {
			e.preventDefault();
			$('body').toggleClass('bulb_on');
		});

 
		$(window).on('load', function () {
			$('.loader').fadeOut();
		});
		// var text = $.trim($('.red_highlight').text()),
		//     word = text.split(' '),
		//     str = "";
		// $.each( word, function( key, value ) {
		//   // if(key != 0) { str += ""; }
		//   str += "<span class='tranistion-delay-"+(parseInt(key)+1)+"'>&nbsp;" + value + "</span>";
		// });
		// $('.red_highlight').html(str);
		$('.section8 .text-table .red_highlight, .section8 .text-table .green_highlight, .section8 .text-table .green_marked, .section8 .text-table .red_marked').each(function(){ 
		    var words = $(this).text().split(/\s+/);
		    var total = words.length;
		    $(this).empty();
		    for (index = 0; index < total; index ++){
		      $(this).append($("<span /> ").html(' '+words[index]+ ' '));
		      }
		});
		$('#clock, #path-99, #path-104, #path-102, #path-105, #path-101').on('click', function () {
			var el = $(this);
			el.removeClass('fast');
			setTimeout(function () {
				el.addClass('fast');
			}, 100);
		});
		$('.last-word-table #group-21').on('click', function () {
			$('.last-word-table').addClass('anim-2');
		});
			Tawk_API.onLoad = function(){
				// $(document).on('mouseleave', function () {
	   //          	if(!$('body').hasClass('popup-in')){
	   //          		Tawk_API.maximize();
	   //          	}
				// });
			};
			Tawk_API.onChatMinimized = function(){
			    $('body').addClass('popup-in');
			};
		// console.log($(".text-table span").size());
		    var ul = $('.section8 .text-table');
		    ul.find('span').addClass(function(index){
		        var group_number = index;
		        return 'tranistion-delay-'+ group_number;
		    });

		
	});
})(jQuery);